function render_timeline(){
  function formatear_fecha(fecha) {
    var dia = ["D","L","M","X","J","V","S"];

    return dia[fecha.getDay()]+" "
      +fecha.getDate();
  }

  function formatear_mes(fecha){
    return (fecha.getMonth()+1)+"/"+fecha.getFullYear();
  }

  function color_cliente(d){
    if(d.cliente === "Cliente A"){
      return d.tarea==="Carga"?
        "rgb(146,175,243)"
        :
        "rgb(60,103,203)";
    }else if(d.cliente === "Cliente B"){
      return d.tarea==="Carga"?
        "rgb(255,51,0)"
        :
        "rgb(191,51,0)";
    }else if(d.cliente === "Cliente C"){
      return d.tarea==="Carga"?
        "rgb(255,255,153)"
        :
        "rgb(242,242,40)";
    }else if(d.cliente === "Entidad validadora"){
      return "rgb(221,235,247)";
    }else {
      return "rgb(90,90,90)";
    }
  }

  var xScale = d3.scale.linear()
    .domain([f_ini.getTime(), f_fin.getTime()+(24*60*60*1000)])
    .range([0,ancho]);

  var fondo_mes = fondo.selectAll(".fondo_mes")
      .data(meses, function(d){ return d.inicio.getTime();});
  fondo_mes.enter()
    .append("svg:rect")
    .attr('class', 'fondo_mes');
  fondo_mes.attr("x", function(d){ return xScale(d.inicio.getTime()) })
    .attr("y", 0)
    .attr("width", function(d) { return xScale(d.fin.getTime()) - xScale(d.inicio.getTime())})
    .attr("height", alto)
    .attr("fill", "#eee")
    .attr("stroke", "#000")
    .attr("stroke-width", 3);
  fondo_mes.exit().remove();

  var lineas_dias = fondo.selectAll(".lineas_dias")
    .data(dias, function(d){ return d.inicio.getTime();});
  lineas_dias.enter()
    .append("svg:line")
    .attr('class', 'lineas_dias');
  lineas_dias.attr("x1", function(d){ return xScale(d.inicio.getTime()) })
    .attr("y1", 2)
    .attr("x2", function(d){ return xScale(d.inicio.getTime()) })
    .attr("y2", alto-2)
    .attr("stroke", "#ccc")
    .attr("stroke-width", 1);
  lineas_dias.exit().remove();

  var nombre_dias = fondo.selectAll(".nombre_dias")
    .data(dias, function(d){ return d.inicio.getTime();});
  nombre_dias.enter()
    .append("svg:text")
    .attr('class', 'nombre_dias');
  nombre_dias.attr("x", function(d){ return xScale(d.inicio.getTime())+10 })
    .attr("y", alto-20)
    .attr("color", "#bbb")
    .attr("font-weight", "bold")
    .text(function(d) { return formatear_fecha(d.inicio) });
  nombre_dias.exit().remove();

  nombres_meses = fondo.selectAll(".nombre_mes")
    .data(meses, function(d){ return d.inicio.getTime();});
  nombres_meses.enter()
      .append("svg:text")
      .attr('class', 'nombre_mes')
  nombres_meses.attr("font-weight","bold")
    .attr("font-size", 24)
    .attr("x", function(d){ return xScale(d.inicio.getTime())+20 })
    .attr("y", 40)
    .text(function(d) {
      return formatear_mes(d.inicio);
    });
  nombres_meses.exit().remove();

  var baseline=linea.selectAll(".baseline")
    .data([1], function(d) {return d;})
  baseline.enter()
    .append("svg:line")
    .attr('class', 'nombre_mes');
  baseline.attr("x1",0)
    .attr("y1",alto/2)
    .attr("x2",ancho)
    .attr("y2",alto/2)
    .attr("stroke", "rgb(255,0,0)")
    .attr("stroke-width", 2);
  baseline.exit().remove();

  var trabajos = linea.selectAll(".trabajos")
    .data(todos_los_trabajos);
  trabajos.enter()
    .append("svg:rect")
    .attr('class', 'trabajos');
  trabajos
    .attr("y", function(d){ return alto/2 + (d.maquina.indexOf('Pre')>-1?-15:1) })
    .attr("width", function(d) { return xScale(d.fin.getTime()) - xScale(d.inicio.getTime())})
    .attr("height", 14)
    .attr("stroke", "#000")
    .attr("stroke-width", 1)
    .attr("fill", color_cliente)
    .transition()
    .duration(1000)
    .attr("x", function(d){ return xScale(d.inicio.getTime()) });
  trabajos.exit()
    .transition()
    .ease("elastic")
    .duration(600)
    .attr("y", -50)
    .remove();

  var horas = linea.selectAll(".fechas")
    .data(todos_los_trabajos);
  horas.enter()
    .append("svg:text")
    .attr('class', 'fechas');
  horas.attr("font-weight","bold")
    .text(function(d) { return d.inicio.getHours() + "h" })
    .attr("y", function(d) { return alto/2 + (d.maquina.indexOf('Pre')>-1?-18:30) })
    .transition()
    .duration(1000)
    .attr("x", function(d) { return xScale(d.inicio.getTime()) });
  horas.exit()
    .style('opacity',"1")
    .attr("x", function(d) { return xScale(d.inicio.getTime()) })
    .attr("y", -50)
    .remove();
}
