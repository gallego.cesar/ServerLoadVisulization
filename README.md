Explicaci�n
===========
En esta visualizaci�n se ilustran las cargas ETL de datos de un cliente que tiene unos procesos de carga muy largos. Para ello usamos una l�nea roja del tiempo de la que cuelgan cajas que representan las cargas de datos. Si la caja se encuentra sobre la l�nea la carga de datos corresponde a Pre-producci�n. Si la caja se encuentra sobre la l�nea corresponde a Producci�n. El ancho de la caja represnta el tiempo necesario para la carga.

La visualicaci�n presenta tres opciones para reducir los tiempos de carga. Optimizaciones mejoradas, Carga en varios d�as y Staging. Cada una de ellas permite tener los datos cargados con diferente rango de d�as. El cliente debe elegir una. 