var carga = function(num_mes) {
  return 8;
};
var optimizacion = function(num_mes) {
  return 12 + (num_mes * 0.5);
};
function generarCargaDia(init, end, dia, tipo_carga, auto){
  var carga;
  if(tipo_carga == "1"){
    carga = optimizaciones_mejoradas;
  }else if (tipo_carga == "3") {
    carga = staging;
  }else{
    carga = carga_en_varios_dias;
  }

	var actuales = [];
	var dias = moment().recur(
		moment(init).startOf('M'),
		moment(end).endOf('M') ).every(dia).dayOfMonth();
	dias.next(4).map(function(m){return m.toDate()})
		.forEach(function(d){
			d.setHours(0);
			actuales = actuales.concat( carga(d,auto) );
		});

	return actuales;
}

function generarMeses(init, end){
	var actuales=[];
	var meses=moment().recur(
		moment(init).startOf('M'),
		moment(end).endOf('M')).every(1).dayOfMonth();

	meses.all()
    .map(function(m){
      return {inicio:new Date(m.startOf('M').toDate()),
        fin:new Date(m.endOf('M').toDate())};
    })
    .forEach(function(d){
      d.inicio.setHours(0);
      d.fin.setHours(0);
      actuales.push(d);
    });

	return actuales;
}

function generarDias(init, end){
	t = new Date(init.getTime());
	//t.setDate(1);
	var t0,t1, dias=[];
	while( t <= end.getTime() ){
		t1=new Date(t.getTime());
		t1.setDate(t1.getDate()+1);
		t1.setHours(23);
		t1.setMinutes(59);
		t0=new Date(t.getTime());
		t0.setHours(0);

		dias.push({inicio:t0, fin:t1});
		t.setDate(t.getDate()+1);
	}
	return dias;
}

function carga_en_varios_dias(d,auto){
  var trabajos = new Array();

  if(!auto){
    while( d.getDay() == 0 || d.getDay() == 6){
      // los sabados y domingos no se inician trabajos
      d.setDate(d.getDate()+1);
    }
    while( d.getDay() == 1 && d.getHours() < 9 ){
      // los lunes no se empiezan las cargas hasta el horario laboral
      d.setHours(d.getHours()+1);
    }
  }

  // inicio carga Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente A',
    maquina:'Pre'});
  // inicio optimizaciones Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente A',
    maquina:'Pre'});
  // inicio carga Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente B',
    maquina:'Pre'});
  // inicio optimizaciones Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente B',
    maquina:'Pre'});
  // inicio carga Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente C',
    maquina:'Pre'});
  // inicio optimizaciones Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente C',
    maquina:'Pre'});
  // pro
  while( d.getDay() == 0 || d.getDay() == 6 || d.getHours() != 9){
    // los sabados y domingos no se valida
    //d.setDate(d.getDate()+1);
    d.setHours(d.getHours()+1);
  }
  // validacion
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + 8);
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Validacion',
    cliente:'Entidad validadora',
    maquina:'Pre'});
  // inicio carga Cliente A
  if(!auto){
    while( d.getDay() == 0 || d.getDay() == 6 || d.getHours() != 21){
      // los sabados y domingos no se inician trabajos
      // en pro solo se trabajo a partir de las 21h
      //d.setDate(d.getDate()+1);
      d.setHours(d.getHours()+1);
    }
  }else{
    // inicio optimizaciones Cliente A
    while( d.getHours() < 21 ){
      // en pro solo se trabajo a partir de las 21h
      d.setHours(d.getHours()+1);
    }
  }

  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente A',
    maquina:'Pro'});
  // inicio optimizaciones Cliente A
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente A',
    maquina:'Pro'});
  // inicio carga Cliente B
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente B',
    maquina:'Pro'});
  // inicio optimizaciones Cliente B
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente B',
    maquina:'Pro'});
  // inicio carga Cliente C
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente C',
    maquina:'Pro'});
  // inicio optimizaciones Cliente C
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente C',
    maquina:'Pro'});

  return trabajos;
}

var optimizacion_parcial = function(num_mes) {
  return 6;
};

function optimizaciones_mejoradas(d,auto){
  var trabajos = new Array();

  if(!auto){
    while( d.getDay() == 0 || d.getDay() == 6){
      // los sabados y domingos no se inician trabajos
      d.setDate(d.getDate()+1);
    }
    while( d.getDay() == 1 && d.getHours() < 9 ){
      // los lunes no se empiezan las cargas hasta el horario laboral
      d.setHours(d.getHours()+1);
    }
  }

  // inicio carga Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente A',
    maquina:'Pre'});
  // inicio optimizaciones Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion_parcial(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente A',
    maquina:'Pre'});
  // inicio carga Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente B',
    maquina:'Pre'});
  // inicio optimizaciones Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion_parcial(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente B',
    maquina:'Pre'});
  // inicio carga Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente C',
    maquina:'Pre'});
  // inicio optimizaciones Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion_parcial(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente C',
    maquina:'Pre'});
  // pro
  while( d.getDay() == 0 || d.getDay() == 6 || d.getHours() != 9){
    // los sabados y domingos no se valida
    //d.setDate(d.getDate()+1);
    d.setHours(d.getHours()+1);
  }
  // validacion
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + 8);
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Validacion',
    cliente:'Entidad validadora',
    maquina:'Pre'});
  // inicio carga Cliente A
  if(!auto){
    while( d.getDay() == 0 || d.getDay() == 6 || d.getHours() != 21){
      // los sabados y domingos no se inician trabajos
      // en pro solo se trabajo a partir de las 21h
      //d.setDate(d.getDate()+1);
      d.setHours(d.getHours()+1);
    }
  }else{
    while( d.getHours() < 21 ){
      // en pro solo se trabajo a partir de las 21h
      d.setHours(d.getHours()+1);
    }
  }

  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente A',
    maquina:'Pro'});
  // inicio optimizaciones Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion_parcial(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente A',
    maquina:'Pro'});
  // inicio carga Cliente B
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente B',
    maquina:'Pro'});
  // inicio optimizaciones Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion_parcial(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente B',
    maquina:'Pro'});
  // inicio carga Cliente C
  while( d.getHours() < 21 ){
    // en pro solo se trabajo a partir de las 21h
    d.setHours(d.getHours()+1);
  }
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente C',
    maquina:'Pro'});
  // inicio optimizaciones Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion_parcial(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente C',
    maquina:'Pro'});

  return trabajos;
}

function staging(d,auto){
  var trabajos = new Array();

  if(!auto){
    while( d.getDay() == 0 || d.getDay() == 6){
      // los sabados y domingos no se inician trabajos
      d.setDate(d.getDate()+1);
    }
    while( d.getDay() == 1 && d.getHours() < 9 ){
      // los lunes no se empiezan las cargas hasta el horario laboral
      d.setHours(d.getHours()+1);
    }
  }

  // inicio carga Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente A',
    maquina:'Pro'});
  // inicio optimizaciones Cliente A
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente A',
    maquina:'Pro'});
  // inicio carga Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente B',
    maquina:'Pro'});
  // inicio optimizaciones Cliente B
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente B',
    maquina:'Pro'});
  // inicio carga Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + carga());
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Carga',
    cliente:'Cliente C',
    maquina:'Pro'});
  // inicio optimizaciones Cliente C
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + optimizacion(d.getMonth()));
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Optimizacion',
    cliente:'Cliente C',
    maquina:'Pro'});
  // volcado a pre
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + 8);
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Volcado',
    cliente:'Sistemas',
    maquina:'Pro'});
  // pro
  while( d.getDay() == 0 || d.getDay() == 6 || d.getHours() != 9){
    // los sabados y domingos no se valida
    //d.setDate(d.getDate()+1);
    d.setHours(d.getHours()+1);
  }
  // validacion
  inicio_tarea = d.getTime();
  d.setHours(d.getHours() + 8);
  trabajos.push({inicio:new Date(inicio_tarea),
    fin:new Date(d.getTime()),
    tarea:'Validacion',
    cliente:'Entidad validadora',
    maquina:'Pre'});

  return trabajos;
}
